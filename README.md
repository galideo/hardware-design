##**About Scrylene Hardware Design**##

This repository contains details about how to build hardware for capturing data for different fruits and vegetables using various environment sensors and camera. 

**Hardware design includes:-**

1. mechanical frame which is designed to let light through in to capture various conditions which can affect the rotting of the fruits. 
2. Electronics design including custom PCB board for the ethylene measurement and data mapping. Apart from it, there is a light sensor, temperature/humidity sensor. photosensor, and raspberry pi microcontroller to capture data repeatedly.

If any queries, please email at aman@galideo.com .